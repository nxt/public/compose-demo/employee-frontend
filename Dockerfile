FROM node:lts-alpine AS dev

WORKDIR /app

# Cache dependencies
COPY package.json yarn.lock ./
RUN yarn install

# Build the app
COPY public public/
COPY src src/
RUN npm run build

ENTRYPOINT ["npm"]
CMD ["start"]

### NEXT STAGE

FROM nginx:alpine AS prod

COPY deployment/nginx/nginx.conf /etc/nginx/nginx.conf
COPY --from=dev /app/build /usr/share/nginx/html
