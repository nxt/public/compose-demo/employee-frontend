import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import $ from 'jquery';

class EmployeeList extends Component {
  constructor(props) {
    super(props);

    this.state = { "employees": [] }
  }

  componentDidMount() {
    this.Employees()
  }

  Employees() {
    let component = this

    $.getJSON("/api/employees")
      .then(function (result) {
        let employeeNames = result['_embedded']['employees'].map((e) => e.name)
        component.setState({"employees": employeeNames})
      })
  }

  render() {
    if (this.state.employees.length === 0) {
      return <ul><li>We're still hiring</li></ul>
    }

    let employeeUls = this.state.employees.map((e) => <li>{e}</li>);

    return <ul>{employeeUls}</ul>
  }
}

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Hello SwissRe.
          </p>
          <p>
            We're <a class="App-link" href="https://nxt.engineering/en/">nxt Engineering GmbH</a>:
          </p>
          <EmployeeList/>
          <p>
            Come say hi <span role="img" aria-label="Waving Hand">👋👋👋</span>!
          </p>
        </header>
      </div>
    );
  }
}

export default App;
